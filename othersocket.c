#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <json-c/json.h>

void LoadConfiguration();
// Data from json file
int freq, sp, dio0, p_reset, r_alt, port1, port2;
float r_lat, r_lon;
char *name[], *email[], *desc[], *server1[], *server2[];
bool *pz, *es1, *es2;

bool pi_zero =  true;
struct sockaddr_in si_other;
int s;
int slen = sizeof(si_other);
struct ifreq ifr;
//struct json_object *server;

int main() {
	s = socket(AF_INET, SOCK_DGRAM, 0);
	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	ifr.ifr_addr.sa_family = AF_INET;
	if (pi_zero){
		strncpy(ifr.ifr_name, "wlx00e6b02017e6", IFNAMSIZ - 1); //wlp3s0  wlx00e6b02017e6
	}
	else{
		strncpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);
	}
	ioctl(s, SIOCGIFHWADDR, &ifr);

	printf( "Gateway ID; %.2x:%.2x:%.2x:ff:ff:%.2x:%.2x:%.2x\n",
		(uint8_t)ifr.ifr_hwaddr.sa_data[0],
		(uint8_t)ifr.ifr_hwaddr.sa_data[1],
		(uint8_t)ifr.ifr_hwaddr.sa_data[2],
		(uint8_t)ifr.ifr_hwaddr.sa_data[3],
		(uint8_t)ifr.ifr_hwaddr.sa_data[4],
		(uint8_t)ifr.ifr_hwaddr.sa_data[5]
	);

	LoadConfiguration();
	printf("puerto: %d\n servidor: %s\n", port1, *server2);
	return 0;
}


void LoadConfiguration(){
	FILE *fp;
	char buffer[65536];

	struct json_object *parsed_json;
	

	fp = fopen("global_conf.json", "r");
	fread(buffer, 1024, 1,fp);
	fclose(fp);
	
	struct json_object *Sx127x, *pfreq, *pspread_factor, *ppin_dio0, *ppin_reset;

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "Sx127x_conf", &Sx127x);
	json_object_object_get_ex(Sx127x, "freq", &pfreq);
	json_object_object_get_ex(Sx127x, "spread_factor", &pspread_factor);
	json_object_object_get_ex(Sx127x, "pin_dio0", &ppin_dio0);
	json_object_object_get_ex(Sx127x, "pin_reset", &ppin_reset);

	freq= json_object_get_int(pfreq);
	sp= json_object_get_int(pspread_factor);
	dio0= json_object_get_int(ppin_dio0);
	p_reset= json_object_get_int(ppin_reset);

	printf("La configuracion para SX127x es:\n Freq: %u\n Spread_Factor: %u\n Pin_dio0: %u\n Pin_reset: %u\n",freq, sp, dio0, p_reset);


	struct json_object *gateway, *pr_lat, *pr_lon, *pr_alt, *pname, *pemail, *pdesc, *ppizero;

	json_object_object_get_ex(parsed_json, "gateway_conf", &gateway);
	json_object_object_get_ex(gateway, "ref_latitude", &pr_lat);
	json_object_object_get_ex(gateway, "ref_longitude", &pr_lon);
	json_object_object_get_ex(gateway, "ref_altitude", &pr_alt);
	json_object_object_get_ex(gateway, "name", &pname);
	json_object_object_get_ex(gateway, "email", &pemail);
	json_object_object_get_ex(gateway, "desc", &pdesc);
	json_object_object_get_ex(gateway, "is_pi_zero", &ppizero);

	r_lat =json_object_get_double(pr_lat);
	r_lon =json_object_get_double(pr_lon);
	r_alt =json_object_get_int(pr_alt);
	*name =json_object_get_string(pname);
	*email =json_object_get_string(pemail);
	*desc =json_object_get_string(pdesc);
	*pz=json_object_get_boolean(ppizero);


	printf("Conf Gateway:\n Latitude: %.3f\n Longitude: %.2f\n Altitude: %u\n Name: %s\n E-mail: %s\n Desc: %s\n Is PiZero: %u\n",
	r_lat, r_lon, r_alt, *name, *email, *desc, *pz);

	struct json_object *servers, *server, *pserver, *pport, *penabled;

	json_object_object_get_ex(gateway, "servers", &servers);

	size_t n_servers;

	n_servers= json_object_array_length(servers);
	printf("Len array: %lu\n", n_servers);

	for (int i=0; i<n_servers; i++)
	{
		server= json_object_array_get_idx(servers, i);
		printf("%u. %s\n", i+1, json_object_get_string(server));
		json_object_object_get_ex(server, "address",&pserver);
		json_object_object_get_ex(server, "port", &pport);
		json_object_object_get_ex(server, "enabled", &penabled);
		switch (i)
		{
		case 0:
			*server1= json_object_get_string(pserver);
			port1= json_object_get_int(pport);
			*es1= json_object_get_boolean(penabled);

			break;
		
		case 1:
			*server2= json_object_get_string(pserver);
			port2= json_object_get_int(pport);
			*es2= json_object_get_boolean(penabled);
			break;
		}
	}
	
}
