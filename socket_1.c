#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdbool.h>
#include <time.h>
#include <fcntl.h> 
#include <errno.h>

#define Protocol_Version 2
#define Pkt_Push_Data 0
#define Pkt_Push_Ack 1
#define Pkt_Pull_Data 2
#define Pkt_Pull_Resp 3
#define Pkt_Pull_Ack 4
#define Pkt_Tx_Ack 5

#define Push_Timeout_Ms 100
#define Pull_Timeout_Ms 200

#define Nb_Packet_Max 8
#define Status_Size 200
#define Tx_Buff_Size ((540*Nb_Packet_Max)+30+Status_Size)

#define Serv_addr "us-west.thethings.network"
#define Serv_port "1700"
#define Id_Gateway "FF12AA34CC56BB78"
#define BUF_SIZE 500

static struct timeval push_timeout_half;
static int sock_up;

int sfd;
static uint64_t lgwm=0; /*Lora Gateway mac address*/
const char *str= Id_Gateway;
unsigned long long ull = 0;
static uint32_t net_mac_msb;
static uint32_t net_mac_lsb;

/*Data Buffers*/
//uint8_t buff_up[Tx_Buff_Size];
int Buff_Index;
uint8_t Buff_Ack[32]; /*buffer to receive acknowledges*/

/*Protocol variables*/
uint8_t token_h;/* random token for acknowledgement matching */
uint8_t token_l;/* random token for acknowledgement matching */

void delay(int number_of_seconds) 
{ 
    // Converting time into milli_seconds 
    int milli_seconds = 1000 * number_of_seconds; 
  
    // Storing start time 
    clock_t start_time = clock(); 
  
    // looping till required time is not achieved 
    while (clock() < start_time + milli_seconds); 
} 

void send_tx_ack(){
    uint8_t Buff_Ack[64];
    int Buff_Index;

    /* start composing datagram with the header */
        token_h = (uint8_t)rand(); /* random token */
        token_l = (uint8_t)rand(); /* random token */

    memset(&Buff_Ack,0, sizeof Buff_Ack);
    /*Prepare downlink feedback to be sent to server*/
    Buff_Ack[0] = Protocol_Version;
    Buff_Ack[1] = token_h;
    Buff_Ack[2] = token_l;
    Buff_Ack[3] = Pkt_Tx_Ack;
    *(u_int32_t *)(Buff_Ack+4) = net_mac_msb;
    *(u_int32_t *)(Buff_Ack+8) = net_mac_lsb;
    Buff_Index=12; /*12 byte header*/
    memcpy((void *)(Buff_Ack+Buff_Index), (void *)"}}",2);
    Buff_Index +=2;

    Buff_Ack[Buff_Index] = 0;
    send(sfd,(void *)Buff_Ack, Buff_Index, 0);

}

void send_pkt(){
    int i, j;
    unsigned pkt_in_dgram; /*nb on Lora packet in the current datagram*/
    
    uint8_t buff_up[Tx_Buff_Size]; /*buufer upstream packet*/
    int buff_index; 
    uint8_t Buff_Ack[32]; /*buffer to receive acknowledges*/

    /*ping measurement variables*/
    struct timespec send_time;
    struct timespec recv_time;
    push_timeout_half.tv_sec = 15;
    push_timeout_half.tv_usec = 0;

    /*set upstream socket RX timeout*/
    i = setsockopt(sock_up, SOL_SOCKET, SO_RCVTIMEO, (void *)&push_timeout_half, sizeof(push_timeout_half));
    if (i != 0){
        printf("ERROR: [up] setsockopt returned %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    token_h = (uint8_t)rand();
    token_l = (uint8_t)rand();
    /*Data Buffer*/
    //buff_up[0] = Protocol_Version;
    buff_up[1] = token_h;
    buff_up[2] = token_l;
    buff_up[3] = Pkt_Push_Data;
    *(uint32_t *)(buff_up+4) = net_mac_msb;
    *(uint32_t *)(buff_up+8) = net_mac_lsb;
    buff_index = 12; /*12 Byte header*/
    
    /* start of JSON structure */
    memcpy((void *)(buff_up + buff_index), (void *)"{\"rxpk\":[", 9);
    buff_index += 9;

    memcpy((void *)(buff_up+buff_index),(void*)"hola",4);
    buff_index += 4;

    buff_up[buff_index]=']';
    ++buff_index;
    buff_up[buff_index]='}';
    ++buff_index;
    /* end of JSON datagram payload */
    buff_up[Buff_Index]= '}';
    ++Buff_Index;
    buff_up[Buff_Index]=0; /* add string terminator, for safety */

    printf("\nJSON up: %s\n", (char *)(buff_up + 12)); /* DEBUG: display JSON payload */

    /* send datagram to server */
    buff_up[0]=Protocol_Version;

    for(int c=0; c<12; c++){
        printf("Buff_up[%d]:%02x.\n",c,buff_up[c]);
    }

    j=send(sock_up, (void *)buff_up, buff_index, 0);
    printf("numero de byte enviados:%d.\n",j);

    clock_gettime(CLOCK_MONOTONIC, &send_time);

    printf("sockup value is %d.\n",sock_up);
    for (i=0; i<2; i++){
        j= recv(sock_up, (void *)Buff_Ack, sizeof Buff_Ack, 0);
        //j=recvfrom(sock_up, (void*)Buff_Ack, sizeof Buff_Ack, 0 , 0, 0);
        //j=read(sock_up,Buff_Ack,32);
        printf("el valor de recv:%d\n",j);
        clock_gettime(CLOCK_MONOTONIC, &recv_time);
        if (j == -1){
            if(errno == EAGAIN){
                printf("aqui error: %d.\n",errno);
                continue;
            }else{
                break;  
            }
        }else if((j<4) || (Buff_Ack[0] != Protocol_Version) || (Buff_Ack[3] != Pkt_Push_Ack)){
            printf("aqui fallo j4.\n");
            continue;
        }else if((Buff_Ack[1] != token_h) || (Buff_Ack[2] != token_l)){
            printf("error tokens.\n");
            continue;
        }else{
            printf("esto funciono\n");
            break;
        }
    }
    printf("\nINFO: End of upstream thread\n");
}


int main(){
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    char host_name[64];
    char port_name[64];
    int s, j;
    size_t len;
    ssize_t nread;
    char buf[BUF_SIZE];

    /*Obtain address matching host/port */
    memset(&hints,0,sizeof hints);
    hints.ai_family= AF_INET; /*Allow IPV4 or IPV6*/
    hints.ai_socktype= SOCK_DGRAM; /*Datagram socket*/
    hints.ai_flags=0;
    hints.ai_protocol=0; /*Any protocol*/
    
    s= getaddrinfo(Serv_addr, Serv_port, &hints, &result);
    if(s != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for(rp=result; rp != NULL; rp= rp->ai_next){
        sock_up= socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sock_up == -1){
            continue;
        }
        else break;
        
    }
    if(rp == NULL){ /*no address succeeded*/
        printf("Error: failed to open socket to any server %s address. port: %s \n", Serv_addr,Serv_port);
        s=1;
        for (rp=result; rp!= NULL; rp=rp->ai_next){
            getnameinfo(rp->ai_addr, rp->ai_addrlen, host_name, sizeof host_name, port_name, sizeof port_name, NI_NUMERICHOST);
            printf("INFO: result %i host:%s service:%s.\n",s,host_name, port_name);
            s++;
        }
        exit(EXIT_FAILURE);
    }

    s=connect(sock_up, rp->ai_addr, rp->ai_addrlen);
    if (s != 0){
        printf("Error: connect returnet %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    //fcntl(sock_up, F_SETFL, O_NONBLOCK);

    freeaddrinfo(result); /*no longer needed*/

    sscanf(str, "%llx", &ull);
    lgwm= ull;
    printf("INFO: gateway MAC %016llX\n",ull);
    /* process some of the configuration variables */
    net_mac_msb = htonl((uint32_t)(0xFFFFFFFF & (lgwm>>32)));
    net_mac_lsb = htonl((uint32_t)(0xFFFFFFFF &  lgwm  ));
    
    send_pkt();
    
}
