#Makefile Socket

objs= socket.o
Libs= -ljson-c
Cflags= -Wall

# Compilar programa principal 
socket: $(objs)
	gcc $(Cflags) -ljson-c -o MainSocket  $(objs) $(Libs)

socket.o: socket.c
	gcc $(Cflags) -c socket.c -o socket.o

clean:
	rm -f MainSocket *.o *.c~ *~
